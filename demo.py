#!/usr/bin/python3

import gi

gi.require_version('Des', '1.0')

from gi.repository import Des, Gio, GLib

server = Des.Notifications.new("org.gnome.zbrown.Des.Notifications",
                               "Zander Brown",
                               "0.1.0")


def changed (list, pos, add, remove):
    noti = list[pos]

    print ('Notification')
    print ('  {}'.format(noti.props.summary))
    print ('  {}'.format(repr(noti.props.urgency)))
    print ('  {}'.format(noti.props.body))
    print ('  {}'.format(noti.props.actions))


server.connect('items-changed', changed)

def got_bus(conn, name):
    print('Got bus')
    server.register(conn)

def lost_name(conn, name):
    print('Didn\'t get {}'.format(name))

Gio.bus_own_name(Gio.BusType.SESSION,
                 "org.freedesktop.Notifications",
                 Gio.BusNameOwnerFlags.REPLACE,
                 got_bus,
                 None,
                 lost_name)

loop = GLib.MainLoop()
loop.run()
