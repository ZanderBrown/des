/* des-notification-private.h
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>
#include <gio/gio.h>

#include "des-notification.h"

G_BEGIN_DECLS

void des_notification_populate (DesNotification   *self,
                                const char        *app_name,
                                guint              replaces_id,
                                const char        *app_icon,
                                const char        *summary,
                                const char        *body,
                                const char *const *actions,
                                GVariant          *hints,
                                int                expire_timeout);

G_END_DECLS
