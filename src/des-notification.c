/* des-notification.c
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "des-notification.h"
#include "des-notification-private.h"

typedef struct _DesNotificationPrivate DesNotificationPrivate;

struct _DesNotificationPrivate {
  GAppInfo      *application;
  GIcon         *icon;
  char          *summary;
  char          *body;
  char         **actions;
  GVariantDict   hints;
  gint32         timeout;
  guint32        id;
  DesUrgency     urgency;
};

G_DEFINE_TYPE_WITH_PRIVATE (DesNotification, des_notification, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_APPLICATION,
  PROP_ICON,
  PROP_SUMMARY,
  PROP_BODY,
  PROP_ACTIONS,
  PROP_TIMEOUT,
  PROP_ID,
  PROP_URGENCY,
  LAST_PROP
};

static GParamSpec *pspecs[LAST_PROP] = { NULL, };

enum {
  HINTS_CHANGED,
  CLOSED,
  ACTION_INVOKED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
des_notification_get_property (GObject       *object,
                               guint          property_id,
                               GValue        *value,
                               GParamSpec    *pspec)
{
  DesNotification *self = DES_NOTIFICATION (object);

  switch (property_id) {
    case PROP_APPLICATION:
      g_value_set_object (value, des_notification_get_application (self));
      break;
    case PROP_ICON:
      g_value_set_object (value, des_notification_get_icon (self));
      break;
    case PROP_SUMMARY:
      g_value_set_string (value, des_notification_get_summary (self));
      break;
    case PROP_BODY:
      g_value_set_string (value, des_notification_get_body (self));
      break;
    case PROP_ACTIONS:
      g_value_set_boxed (value, des_notification_get_actions (self));
      break;
    case PROP_TIMEOUT:
      g_value_set_int (value, des_notification_get_timeout (self));
      break;
    case PROP_ID:
      g_value_set_uint (value, des_notification_get_id (self));
      break;
    case PROP_URGENCY:
      g_value_set_enum (value, des_notification_get_urgency (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
des_notification_finalize (GObject *object)
{
  DesNotification *self = DES_NOTIFICATION (object);
  DesNotificationPrivate *priv = des_notification_get_instance_private (self);

  g_clear_object (&priv->application);
  g_clear_object (&priv->icon);
  g_clear_pointer (&priv->summary, g_free);
  g_clear_pointer (&priv->body, g_free);
  g_clear_pointer (&priv->actions, g_strfreev);

  g_variant_dict_clear (&priv->hints);

  G_OBJECT_CLASS (des_notification_parent_class)->finalize (object);
}

static void
des_notification_class_init (DesNotificationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = des_notification_get_property;
  object_class->finalize = des_notification_finalize;

  signals[HINTS_CHANGED] = g_signal_new ("hints-changed",
                                         G_TYPE_FROM_CLASS (klass),
                                         G_SIGNAL_RUN_LAST,
                                         0,
                                         NULL, NULL, NULL,
                                         G_TYPE_NONE, 0);
  signals[CLOSED] = g_signal_new ("closed",
                                  G_TYPE_FROM_CLASS (klass),
                                  G_SIGNAL_RUN_LAST,
                                  0,
                                  NULL, NULL, NULL,
                                  G_TYPE_NONE, 1, G_TYPE_INT);
  signals[ACTION_INVOKED] = g_signal_new ("action-invoked",
                                          G_TYPE_FROM_CLASS (klass),
                                          G_SIGNAL_RUN_LAST,
                                          0,
                                          NULL, NULL, NULL,
                                          G_TYPE_NONE, 1, G_TYPE_STRING);

  pspecs[PROP_APPLICATION] = g_param_spec_object ("application",
                                                  "Application",
                                                  "Notification source",
                                                  G_TYPE_APP_INFO,
                                                  G_PARAM_READABLE);

  pspecs[PROP_ICON] = g_param_spec_object ("icon",
                                           "Icon",
                                           "Notification icon",
                                           G_TYPE_ICON,
                                           G_PARAM_READABLE);

  pspecs[PROP_SUMMARY] = g_param_spec_string ("summary",
                                              "Summary",
                                              "Notification summary",
                                              NULL,
                                              G_PARAM_READABLE);

  pspecs[PROP_BODY] = g_param_spec_string ("body",
                                           "Body",
                                           "Notification body",
                                           NULL,
                                           G_PARAM_READABLE);

  pspecs[PROP_ACTIONS] = g_param_spec_boxed ("actions",
                                             "Actions",
                                             "Notification actions",
                                             G_TYPE_STRV,
                                             G_PARAM_READABLE);

  pspecs[PROP_TIMEOUT] = g_param_spec_int ("timeout",
                                           "Timeout",
                                           "Notification timeout",
                                           -1, G_MAXINT32, -1,
                                           G_PARAM_READABLE);

  pspecs[PROP_ID] = g_param_spec_uint ("id",
                                       "ID",
                                       "Notification id",
                                       0, G_MAXUINT32, 0,
                                       G_PARAM_READABLE);

  pspecs[PROP_URGENCY] = g_param_spec_enum ("urgency",
                                            "Urgency",
                                            "Notification urgency",
                                            DES_TYPE_URGENCY,
                                            DES_URGENCY_NORMAL,
                                            G_PARAM_READABLE);

  g_object_class_install_properties (object_class, LAST_PROP, pspecs);
}

static void
des_notification_init (DesNotification *self)
{
  DesNotificationPrivate *priv = NULL;

  priv = des_notification_get_instance_private (self);
  priv->application = NULL;
  priv->icon = NULL;
  priv->summary = NULL;
  priv->body = NULL;
  priv->actions = NULL;
  g_variant_dict_init (&priv->hints, NULL);
  priv->timeout = -1;
  priv->id = 0;
  priv->urgency = DES_URGENCY_NORMAL;
}

/**
 * des_notification_get_application:
 * @self: the #DesNotification
 * 
 * Returns: (transfer none): the sender #GAppInfo
 */
GAppInfo *
des_notification_get_application (DesNotification *self)
{
  DesNotificationPrivate *priv = NULL;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (DES_IS_NOTIFICATION (self), NULL);

  priv = des_notification_get_instance_private (self);

  return priv->application;
}

/**
 * des_notification_get_icon:
 * @self: the #DesNotification
 * 
 * Returns: (transfer none): the #GIcon to show for the notification
 */
GIcon *
des_notification_get_icon (DesNotification *self)
{
  DesNotificationPrivate *priv = NULL;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (DES_IS_NOTIFICATION (self), NULL);

  priv = des_notification_get_instance_private (self);

  return priv->icon;
}

const char *
des_notification_get_summary (DesNotification *self)
{
  DesNotificationPrivate *priv = NULL;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (DES_IS_NOTIFICATION (self), NULL);

  priv = des_notification_get_instance_private (self);

  return priv->summary;
}

const char *
des_notification_get_body (DesNotification *self)
{
  DesNotificationPrivate *priv = NULL;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (DES_IS_NOTIFICATION (self), NULL);

  priv = des_notification_get_instance_private (self);

  return priv->body;
}

const char *const*
des_notification_get_actions (DesNotification *self)
{
  DesNotificationPrivate *priv = NULL;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (DES_IS_NOTIFICATION (self), NULL);

  priv = des_notification_get_instance_private (self);

  return (const char *const*) priv->actions;
}

gint32
des_notification_get_timeout (DesNotification *self)
{
  DesNotificationPrivate *priv = NULL;

  g_return_val_if_fail (self != NULL, -1);
  g_return_val_if_fail (DES_IS_NOTIFICATION (self), -1);

  priv = des_notification_get_instance_private (self);

  return priv->timeout;
}

guint32
des_notification_get_id (DesNotification *self)
{
  DesNotificationPrivate *priv = NULL;

  g_return_val_if_fail (self != NULL, 0);
  g_return_val_if_fail (DES_IS_NOTIFICATION (self), 0);

  priv = des_notification_get_instance_private (self);

  return priv->id;
}

GVariant *
des_notification_get_hint (DesNotification *self,
                           const char      *key)
{
  DesNotificationPrivate *priv = NULL;
  GVariant *value = NULL;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (DES_IS_NOTIFICATION (self), NULL);
  g_return_val_if_fail (key != NULL, NULL);

  priv = des_notification_get_instance_private (self);

  value = g_variant_dict_lookup_value (&priv->hints,
                                       key,
                                       G_VARIANT_TYPE_VARIANT);

  return value;
}

DesUrgency
des_notification_get_urgency (DesNotification *self)
{
  DesNotificationPrivate *priv = NULL;

  g_return_val_if_fail (self != NULL, DES_URGENCY_NORMAL);
  g_return_val_if_fail (DES_IS_NOTIFICATION (self), DES_URGENCY_NORMAL);

  priv = des_notification_get_instance_private (self);

  return priv->urgency;
}

static void
handle_hints (DesNotification *self,
              GVariant        *hints)
{
  DesNotificationPrivate *priv = des_notification_get_instance_private (self);
  DesUrgency urgency = DES_URGENCY_NORMAL;

  g_variant_dict_clear (&priv->hints);

  g_variant_dict_init (&priv->hints, hints);

  if (g_variant_dict_lookup (&priv->hints, "urgency", "y", &urgency)) {
    if (urgency != priv->urgency) {
      g_object_notify_by_pspec (G_OBJECT (self), pspecs[PROP_URGENCY]);
    }
  }
}

// Introduced in 2.60.0 but we only have 2.58 available
static gboolean
_g_strv_equal (const gchar * const *strv1,
              const gchar * const *strv2)
{
  g_return_val_if_fail (strv1 != NULL, FALSE);
  g_return_val_if_fail (strv2 != NULL, FALSE);

  if (strv1 == strv2)
    return TRUE;

  for (; *strv1 != NULL && *strv2 != NULL; strv1++, strv2++)
    {
      if (!g_str_equal (*strv1, *strv2))
        return FALSE;
    }

  return (*strv1 == NULL && *strv2 == NULL);
}

void
des_notification_populate (DesNotification   *self,
                           const char        *app_name,
                           guint              replaces_id,
                           const char        *app_icon,
                           const char        *summary,
                           const char        *body,
                           const char *const *actions,
                           GVariant          *hints,
                           int                expire_timeout)
{
  DesNotificationPrivate *priv = NULL;

  g_return_if_fail (self != NULL);
  g_return_if_fail (DES_IS_NOTIFICATION (self));

  priv = des_notification_get_instance_private (self);

  if (g_strcmp0 (priv->summary, summary) != 0) {
    g_clear_pointer (&priv->summary, g_free);

    priv->summary = g_strdup (summary);

    g_object_notify_by_pspec (G_OBJECT (self), pspecs[PROP_SUMMARY]);
  }

  if (g_strcmp0 (priv->body, body) != 0) {
    g_clear_pointer (&priv->body, g_free);

    priv->body = g_strdup (body);

    g_object_notify_by_pspec (G_OBJECT (self), pspecs[PROP_BODY]);
  }

  if (priv->actions == NULL || 
      !_g_strv_equal ((const char *const *) priv->actions, actions)) {
    g_clear_pointer (&priv->actions, g_strfreev);

    priv->actions = g_boxed_copy (G_TYPE_STRV, actions);

    g_object_notify_by_pspec (G_OBJECT (self), pspecs[PROP_ACTIONS]);
  }

  handle_hints (self, hints);
}
