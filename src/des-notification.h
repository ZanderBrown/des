/* des-notification.h
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>
#include <gio/gio.h>

#include "des-enums.h"

G_BEGIN_DECLS

typedef enum    /*< enum,prefix=DES >*/
{
  DES_URGENCY_LOW = 0,    /*< nick=low >*/
  DES_URGENCY_NORMAL = 1, /*< nick=normal >*/
  DES_URGENCY_HIGH = 2,   /*< nick=high >*/
} DesUrgency;

#define DES_TYPE_NOTIFICATION (des_notification_get_type())

G_DECLARE_DERIVABLE_TYPE (DesNotification, des_notification, DES, NOTIFICATION, GObject)

struct _DesNotificationClass {
  GObjectClass parent_class;
};

GAppInfo          *des_notification_get_application (DesNotification *self);
GIcon             *des_notification_get_icon        (DesNotification *self);
const char        *des_notification_get_summary     (DesNotification *self);
const char        *des_notification_get_body        (DesNotification *self);
const char *const *des_notification_get_actions     (DesNotification *self);
GVariant          *des_notification_get_hint        (DesNotification *self,
                                                     const char      *key);
gint32             des_notification_get_timeout     (DesNotification *self);
guint32            des_notification_get_id          (DesNotification *self);
DesUrgency         des_notification_get_urgency     (DesNotification *self);
void               des_notification_close           (DesNotification *self);
void               des_notification_invoke_action   (DesNotification *self);

G_END_DECLS
