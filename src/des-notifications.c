/* des-notification.c
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "des-notifications.h"
#include "des-notification-private.h"

typedef struct _DesNotificationsPrivate DesNotificationsPrivate;

struct _DesNotificationsPrivate {
  DesFdNotifications *bus_object;

  GSequence *items;

  /* cache */
  struct {
    gboolean       is_valid;
    guint          position;
    GSequenceIter *iter;
  } last;

  char *server_name;
  char *server_vendor;
  char *server_version;
};

static void list_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (DesNotifications, des_notifications, G_TYPE_OBJECT,
                         G_ADD_PRIVATE (DesNotifications)
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_iface_init))

enum {
  PROP_0,
  PROP_SERVER_NAME,
  PROP_SERVER_VENDOR,
  PROP_SERVER_VERSION,
  LAST_PROP
};

static GParamSpec *pspecs[LAST_PROP] = { NULL, };

static void
des_notifications_get_property (GObject       *object,
                                guint          property_id,
                                GValue        *value,
                                GParamSpec    *pspec)
{
  DesNotifications *self = DES_NOTIFICATIONS (object);
  DesNotificationsPrivate *priv = des_notifications_get_instance_private (self);

  switch (property_id) {
    case PROP_SERVER_NAME:
      g_value_set_string (value, priv->server_name);
      break;
    case PROP_SERVER_VENDOR:
      g_value_set_string (value, priv->server_vendor);
      break;
    case PROP_SERVER_VERSION:
      g_value_set_string (value, priv->server_version);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
des_notifications_set_property (GObject       *object,
                          guint          property_id,
                          const GValue  *value,
                          GParamSpec    *pspec)
{
  DesNotifications *self = DES_NOTIFICATIONS (object);
  DesNotificationsPrivate *priv = des_notifications_get_instance_private (self);

  switch (property_id) {
    case PROP_SERVER_NAME:
      priv->server_name = g_value_dup_string (value);
      break;
    case PROP_SERVER_VENDOR:
      priv->server_vendor = g_value_dup_string (value);
      break;
    case PROP_SERVER_VERSION:
      priv->server_version = g_value_dup_string (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
des_notifications_finalize (GObject *object)
{
  DesNotifications *self = DES_NOTIFICATIONS (object);
  DesNotificationsPrivate *priv = des_notifications_get_instance_private (self);

  g_clear_object (&priv->bus_object);

  g_clear_pointer (&priv->items, g_sequence_free);
  g_clear_pointer (&priv->server_name, g_free);
  g_clear_pointer (&priv->server_vendor, g_free);
  g_clear_pointer (&priv->server_version, g_free);

  G_OBJECT_CLASS (des_notifications_parent_class)->finalize (object);
}

static void
des_notifications_class_init (DesNotificationsClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = des_notifications_get_property;
  object_class->set_property = des_notifications_set_property;
  object_class->finalize = des_notifications_finalize;

  pspecs[PROP_SERVER_NAME] = g_param_spec_string ("server-name",
                                                  "Server name",
                                                  "Name of the server",
                                                  NULL,
                                                  G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  pspecs[PROP_SERVER_VENDOR] = g_param_spec_string ("server-vendor",
                                                    "Server vendor",
                                                    "Vendor of the server",
                                                    NULL,
                                                    G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  pspecs[PROP_SERVER_VERSION] = g_param_spec_string ("server-version",
                                                     "Server version",
                                                     "Version of the server",
                                                     NULL,
                                                     G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);

  g_object_class_install_properties (object_class, LAST_PROP, pspecs);
}

static GType
list_get_item_type (GListModel *list)
{
  return G_TYPE_APP_INFO;
}

static gpointer
list_get_item (GListModel *list, guint position)
{
  DesNotifications *self = DES_NOTIFICATIONS (list);
  DesNotificationsPrivate *priv = des_notifications_get_instance_private (self);
  GSequenceIter *it = NULL;

  if (priv->last.is_valid)
    {
      if (position < G_MAXUINT && priv->last.position == position + 1)
        it = g_sequence_iter_prev (priv->last.iter);
      else if (position > 0 && priv->last.position == position - 1)
        it = g_sequence_iter_next (priv->last.iter);
      else if (priv->last.position == position)
        it = priv->last.iter;
    }

  if (it == NULL)
    it = g_sequence_get_iter_at_pos (priv->items, position);

  priv->last.iter = it;
  priv->last.position = position;
  priv->last.is_valid = TRUE;

  if (g_sequence_iter_is_end (it))
    return NULL;
  else
    return g_object_ref (g_sequence_get (it));

}

static unsigned int
list_get_n_items (GListModel *list)
{
  DesNotifications *self = DES_NOTIFICATIONS (list);
  DesNotificationsPrivate *priv = des_notifications_get_instance_private (self);

  return g_sequence_get_length (priv->items);
}

static void
list_iface_init (GListModelInterface *iface)
{
  iface->get_item_type = list_get_item_type;
  iface->get_item = list_get_item;
  iface->get_n_items = list_get_n_items;
}

static gboolean
fd_notifications_close_notification (DesFdNotifications    *object,
                                     GDBusMethodInvocation *invocation,
                                     guint                  arg_id)
{
  return FALSE;
}

static gboolean
fd_notifications_get_capabilities (DesFdNotifications    *object,
                                   GDBusMethodInvocation *invocation,
                                   DesNotifications      *self)
{
  const char *capabilities[] = {
    "actions",
    "body",
    "body-markup",
    "icon-static",
    NULL
  };

  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (DES_IS_NOTIFICATIONS (self), FALSE);

  des_fd_notifications_complete_get_capabilities (object,
                                                  invocation,
                                                  capabilities);

  return TRUE;
}

static gboolean
fd_notifications_get_server_information (DesFdNotifications    *object,
                                         GDBusMethodInvocation *invocation,
                                         DesNotifications      *self)
{
  DesNotificationsPrivate *priv = NULL;

  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (DES_IS_NOTIFICATIONS (self), FALSE);

  priv = des_notifications_get_instance_private (self);

  des_fd_notifications_complete_get_server_information (object,
                                                        invocation,
                                                        priv->server_name,
                                                        priv->server_vendor,
                                                        priv->server_version,
                                                        "1.2");

  return TRUE;
}

static int
compare_func (gconstpointer a,
              gconstpointer b,
              gpointer      user_data)
{
  DesUrgency urgency_a = 0;
  DesUrgency urgency_b = 0;
  
  urgency_a = des_notification_get_urgency (DES_NOTIFICATION ((gpointer) a));
  urgency_b = des_notification_get_urgency (DES_NOTIFICATION ((gpointer) b));

  return urgency_b - urgency_a;
}

static gboolean
fd_notifications_notify (DesFdNotifications    *object,
                         GDBusMethodInvocation *invocation,
                         const gchar           *arg_app_name,
                         guint                  arg_replaces_id,
                         const gchar           *arg_app_icon,
                         const gchar           *arg_summary,
                         const gchar           *arg_body,
                         const gchar *const    *arg_actions,
                         GVariant              *arg_hints,
                         gint                   arg_expire_timeout,
                         DesNotifications      *self)
{
  DesNotificationsPrivate *priv = NULL;
  DesNotification *noti = NULL;
  GSequenceIter *iter = NULL;

  g_return_val_if_fail (self != NULL, FALSE);
  g_return_val_if_fail (DES_IS_NOTIFICATIONS (self), FALSE);

  priv = des_notifications_get_instance_private (self);

  noti = g_object_new (DES_TYPE_NOTIFICATION, NULL);

  des_notification_populate (noti,
                             arg_app_name,
                             arg_replaces_id,
                             arg_app_icon,
                             arg_summary,
                             arg_body,
                             arg_actions,
                             arg_hints,
                             arg_expire_timeout);

  iter = g_sequence_insert_sorted (priv->items, noti, compare_func, NULL);

  g_list_model_items_changed (G_LIST_MODEL (self),
                              g_sequence_iter_get_position (iter),
                              0,
                              1);

  des_fd_notifications_complete_notify (object, invocation, 0 /* TODO */);

  return TRUE;
}

static void
des_notifications_init (DesNotifications *self)
{
  DesNotificationsPrivate *priv = des_notifications_get_instance_private (self);

  priv->bus_object = des_fd_notifications_skeleton_new ();

  priv->items = g_sequence_new ((GDestroyNotify) g_object_unref);

  priv->server_name = NULL;
  priv->server_vendor = NULL;
  priv->server_version = NULL;

  g_signal_connect (priv->bus_object,
                    "handle-close-notification",
                    G_CALLBACK (fd_notifications_close_notification),
                    self);
  g_signal_connect (priv->bus_object,
                    "handle-get-capabilities",
                    G_CALLBACK (fd_notifications_get_capabilities),
                    self);
  g_signal_connect (priv->bus_object,
                    "handle-get-server-information",
                    G_CALLBACK (fd_notifications_get_server_information),
                    self);
  g_signal_connect (priv->bus_object,
                    "handle-notify",
                    G_CALLBACK (fd_notifications_notify),
                    self);
}

DesNotifications *
des_notifications_new (const char *name,
                       const char *vendor,
                       const char *version)
{
  return g_object_new (DES_TYPE_NOTIFICATIONS,
                       "server-name", name,
                       "server-vendor", vendor,
                       "server-version", version,
                       NULL);
}

void
des_notifications_register (DesNotifications  *self,
                            GDBusConnection   *connection,
                            GError           **error)
{
  DesNotificationsPrivate *priv = des_notifications_get_instance_private (self);
  
  g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (priv->bus_object),
                                    connection,
                                    "/org/freedesktop/Notifications",
                                    error);
}
