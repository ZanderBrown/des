/* des-notifications.h
 *
 * Copyright 2019 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <glib-object.h>
#include <gio/gio.h>

#include "des-notification.h"
#include "des-dbus.h"

G_BEGIN_DECLS

#define DES_TYPE_NOTIFICATIONS (des_notifications_get_type())

G_DECLARE_DERIVABLE_TYPE (DesNotifications, des_notifications, DES, NOTIFICATIONS, GObject)

struct _DesNotificationsClass {
  GObjectClass parent_class;
};

DesNotifications *des_notifications_new      (const char        *name,
                                              const char        *vendor,
                                              const char        *version);
void              des_notifications_register (DesNotifications  *self,
                                              GDBusConnection   *connection,
                                              GError           **error);

G_END_DECLS
